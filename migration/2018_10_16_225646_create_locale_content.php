<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocaleContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locale_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('target')->unsigned()->comment('標的')->index();
            $table->string('locale')->comment('語系')->index();
            $table->string('content')->comment('值');
            $table->timestamps();

            $table->unique(['target','locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locale_contents');
    }
}
