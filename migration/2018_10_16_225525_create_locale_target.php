<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocaleTarget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locale_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->comment('多語系代號');
            $table->integer('parent')->unsigned()->comment('上一層')->default(0);
            $table->boolean('is_leaf')->comment('是否為末端');
            $table->timestamps();

            $table->unique(['parent','code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locale_targets');
    }
}
