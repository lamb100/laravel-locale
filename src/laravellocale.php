<?php
return [
	'root'=> 'site',
	'supported'=>[
		'en',
		'zh-tw'
	],
	'scan_path'=>[
		'app/Http/Controller/',
		'resources/views/'
	]
];
?>