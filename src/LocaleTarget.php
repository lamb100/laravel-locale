<?php

namespace Lamb100\LaravelLocale;

use Illuminate\Database\Eloquent\Model;

class LocaleTarget extends Model
{
    //
    protected $table="locale_targets";
    protected $casts=[
        'is_leaf'=>'boolean'
    ];

    public function theContents(){
    	return $this->hasMany('App\LocaleContent','target','id');
    }

    public function theParent(){
    	return $this->belongsTo('App\LocaleTarget','parent','id');
    }

    public function theChildren(){
    	return $this->hasMany('App\LocaleTarget','parent','id');
    }
}
