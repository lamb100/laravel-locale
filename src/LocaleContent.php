<?php

namespace Lamb100\LaravelLocale;

use Illuminate\Database\Eloquent\Model;

class LocaleContent extends Model
{
    //
    protected $table='locale_contents';

    public function theTarget(){
    	return $this->belongsTo('App\LocaleTarget','target','id');
    }
}
