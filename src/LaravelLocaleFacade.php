<?php
namespace Lamb100\LaravelLocale;

use Illuminate\Support\Facades\Facade;

class LaravelLocaleFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'LaravelLocale';
    }
}
?>