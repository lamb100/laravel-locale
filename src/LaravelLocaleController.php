<?php

namespace Lamb100\LaravelLocale;

use Illuminate\Http\Request;
use Lamb100\LaravelLocale\LocaleTarget as LocaleTarget;
use Lamb100\LaravelLocale\LocaleContent as LocaleContent;

class LaravelLocaleController extends \App\Http\Controllers\Controller
{
	protected $root,$supported,$scan_path;
	public function __construct(){
		$this->root=config('LaravelLocale.root');
		$this->supported=config('LaravelLocale.supported');
		$this->scan_path=config('LaravelLocale.scan_path');
	}
	/**
	 * route('locale.list')
	 * @param string $locale (optional) locale
	 * @return Illuminate\Http\Response
	 */
	public function viewList($locale=null){
		if(!empty($locale)){
			$supported=config('LaravelLocale.supported');
			$root=config('LaravelLocale.root');
			if(!in_array($locale,$supported)){
				throw new \ErrorException(__("{$root}.error.not_support",['locale'=>$locale,'supported'=>implode(',',$supported)]));
				return false;
			}else{
				App::setLocale($locale);
			}
		}
		$targets=LocaleTarget::where('is_leaf',true)
			->orderBy('parent','asc')
			->orderBy('code','asc')
			->orderBy('create_at','desc')->get();
		$param=['data'=>[],'supported'=>config('LaravelLocale.supported')];
		foreach($targets as $t){
			$contents=$t->theContents;
			$contentForSet=[];
			foreach($contents as $c){
				$contentForSet[$c->locale]=$c->content;
			}
			$param['data'][]=[
				'code'=>$this->getLocaleCode($t),
				'content'=>$contentForSet
			];
		}
		return response()->view('vendor.lamb100.laravellocale.list',$param)->header('Content-Encode','compress');
	}
	/**
	 * 取得語言代碼的全稱
	 * @param Lamb100\LaravelLocale\LocaleTarget $t
	 * @param string $postfix
	 * 
	 * @return string
	 */
	protected function getLocaleCode(LocaleTarget $t,$postfix=""){
		$root=config('LaravelLocale.root');
		if(empty($t->parent)){
			return "{$root}.{$t->code}.{$postfix}";
		}else{
			return $this->getLocaleCode($t->theParent,"{$t->code}.{$postfix}");
		}
	}
	/**
	 * 設定Locale
	 * route('locale.set')
	 * 
	 * @param Request $r
	 *
	 * @return Illuminate\Http\Response
	 */
	public function setLocale(Request $r){
		$this->realSet([
			'code'=>$r->code,
			'locale'=>$r->locale,
			'content'=>$r->content,
		]);
		return response()->json([
			's'=>1
		])->header('Content-Encode','compress');
	}
	/**
	 * 真正設定Locale
	 * route('locale.set')
	 * 
	 * @param string[] $data
	 *
	 * @return null
	 */
	protected function realSet($data){
		$parent=$this->getParent($data['code']);
		$aCode=explode('.',$data['code']);
		$code=array_pop($aCode);
		//先檢查code是否存在
		$t=LocaleTarget::where([
			'parent'=>$parent,
			'code'=>$code
		]);
		if($t->count()<=0){
			$t=new LocaleTarget();
			$t->parent=$parent;
			$t->code=$code;
			$t->is_leaf=1;
			$t->save();
		}else{
			$t=$t->first();
		}
		if(!in_array($data['locale'], $this->supported)){
			throw new \ErrorException(__("{$root}.error.not_support",['locale'=>$data['locale'],'supported'=>implode(',',$supported)]));
			return false;
		}
		$c=LocaleContent::where([
			'target'=>$t->id,
			'locale'=>$data['locale']
		]);
		if($c->count()<=0){
			$c=new LocaleContent();
			$c->target=$t->id;
			$c->locale=$data['locale'];
		}else{
			$c=$c->first();
		}
		$c->content=$data['content'];
		$c->save();
		$this->genLocale($r->locale);
	}
	/**
	 * 取得某一個語言代碼的代號
	 * @param string $code
	 * @param boolean $getParent
	 *
	 * @return Illuminate\Http\Response
	 */
	protected function getParent($code,$getParent=true){
		$aCode=explode('.',$code);
		$iParent=0;
		$root=config('LaravelLocale.root');
		foreach($aCode as $k0=>$v0){
			$isLeaf=0;
			if($k0==0){
				if($v0!=$root){
					throw new \ErrorException(__("{$root}.error.bad_root",['given'=>$v0]));
					return;
				}
			}
			if($getParent&&$k0==(count($aCode)-1)){
				continue;
			}else if(!$getParent&&$k0==(count($aCode)-1)){
				$isLeaf=1;
			}
			//先檢查這層的code是否存在
			$t=LocaleTarget::where([
				'parent'=>$iParent,
				'code'=>$v0
			]);
			//不存在:新增
			if($t->count()<=0){
				$t=new LocaleTarget();
				$t->parent=$iParent;
				$t->code=$v0;
				$t->is_leaf=$isLeaf;
			//存在:取得資料並強制將is_leaf的欄位轉成false
			}else{
				$t=$t->first();
				$t->is_leaf=$isLeaf;
			}
			$t->save();
			$iParent=$t->id;
		}
		return $iParent;
	}
	/**
	 * 生成語言檔資料
	 * route('locale.generate')
	 *
	 * @param string $locale
	 *
	 * @return Illuminate\Http\Response
	 */
	public function genLocale($locale=""){
		if(empty($locale)){
			$locale=App::getLocale();
		}
		if(!in_array($locale, $this->supported)){
			throw new \ErrorException(__("{$root}.error.not_support",['locale'=>$locale,'supported'=>implode(',',$supported)]));
			return false;
		}
		$t=LocaleTarget::orderBy('parent','asc')
			->orderBy('is_leaf','asc')
			->orderBy('id','asc');
		if($t->count()>0){
			$data=[];
			$ts=$t->get();
			foreach($ts as $t){
				if($t->is_leaf){
					if($t->theContents->where(['locale'=>$locale])->count()>0){
						$data[$t->id]=$t->theContents->where(['locale'=>$locale])->first()->content;
					}else{
						$data[$t->id]='';
					}
				}else{
					$data[$t->id]=[];
					$data[$t->parent][$t->id]=&$data[$t->id];
				}
			}
			$sArray=json_encode($data[0],JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK|JSON_PRETTY_PRINT|JSON_THROW_ON_ERROR);
			$sCode="<?php\n return json_decode('{$sArray}',1); ?>";
			$sFile=App::basePath().'/resources/lang/{$locale}/{$this->root}.php';
			if(!file_exists(dirname($sFile))){
				mkdir(dirname($sFile),'0777',true);
			}
			if(!is_dir(dirname($sFile))){
				unlink(dirname($sFile));
				mkdir(dirname($sFile),'0777',true);
			}
			@file_put_contents($sFile, $sCode);
			return response()->json([
				's'=>1
			])->header('Content-Encode','compress');
		}else{
			throw new \ErrorException(__("{$root}.error.not_locale",['locale'=>$locale]));
		}
	}
	/**
	 * 生成語言檔資料樣版
	 * route('locale.template')
	 *
	 * @return Illuminate\Http\Response
	 */
	public function genTemplate(){
		$t=LocaleTarget::orderBy('parent','asc')
			->orderBy('is_leaf','asc')
			->orderBy('id','asc');
		if($t->count()>0){
			$data=[];
			$ts=$t->get();
			foreach($ts as $t){
				if($t->is_leaf){
					$data[$t->id]='';
				}else{
					$data[$t->id]=[];
					$data[$t->parent][$t->id]=&$data[$t->id];
				}
			}
			$sArray=json_encode($data[0],JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK|JSON_PRETTY_PRINT|JSON_THROW_ON_ERROR);
			$sCode="<?php\n return json_decode('{$sArray}',1); ?>";
			return response($Code)->header([
				'Content-Encode'=>'compress',
				'Content-Type' => 'text/plain;charset=utf-8'
			]);
		}else{
			throw new \ErrorException(__("{$root}.error.not_locale",['locale'=>$locale]));
		}
	}

	/**
	 * 檢查需要設定語系的代碼
	 * route('locale.scan')
	 *
	 * @param string[] $scanDir optional
	 *
	 * @return Illuminate\Http\Response
	 */
	public function scanLocale($scanDir=[]){
		if(empty($scanDir)){
			$scanDir=$this->scan_path;
		}
		$nextSacn=$codes=[];
		foreach($scanDir as $d){
			$dir=realpath($d);
			if(!$dir){
				continue;
			}
			$rD=opendir($dir);
			while(false!==($p=readdir())){
				if(is_dir("{$dir}/{$p}")){
					$nextSacn[]="{$dir}/{$p}";
					continue;
				}
				$content=file_get_contents("{$dir}/{$p}");
				preg_match_all('#'.$this->root.'(\.[0-9a-z\_]+)+#',$content,$matches);
				$codes=array_merge($codes,$matches[0]);
				$codes=array_unique($codes);
			}
		}
		$this->scanLocale($nextSacn);
		foreach($codes as $code){
			$this->getParent($code,false);
		}
		return response()->json([
			's'=>1
		])->header('Content-Encode','compress');
	}


	/**
	 * 同步語言檔
	 * route('locale.scan')
	 *
	 * @param string $locale optional
	 *
	 * @return Illuminate\Http\Response
	 */
	public function syncFromFile($locale=''){
		if(empty($locale)){
			$locale=App::getLocale();
		}
		if(!in_array($locale, $this->supported)){
			throw new \ErrorException(__("{$root}.error.not_support",['locale'=>$locale,'supported'=>implode(',',$supported)]));
			return false;
		}
		$sFile=App::basePath().'/resources/lang/{$locale}/{$this->root}.php';
		$locales=require($sFile);
		$this->realSync($locale);
		return response($Code)->header([
			'Content-Encode'=>'compress',
			'Content-Type' => 'text/plain;charset=utf-8'
		]);
	}

	/**
	 * 正式同步語言
	 *
	 * @param array $array
	 * @param string $prefix optional
	 *
	 * @return null
	 */
	protected function realSync($array,$prefix=""){
		if(empty($prefix)){
			$prefix=$this->root;
		}
		foreach($array as $k=>$a){
			if(is_array($a)){
				$this->realSync($a,"{$prefix}.{$k}");
				continue;
			}
			$this->realSet([
				'code'=>"{$prefix}.{$k}",
				'locale'=>App::getLocale(),
				'content'=>$a
			]);
		}
	}
}
?>