<?php

namespace Lamb100\LaravelLocale;

use Illuminate\Support\ServiceProvider;

class LaravelLocaleProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->make(LaravelLocaleController::class);
        $this->loadViewsFrom(__DIR__.'/../views','LaravelLocale');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/migration');
        $this->mergeConfigFrom(__DIR__.'/laravellocale.php','LaravelLocale');
        $this->publishes([
            __DIR__."/laravellocale.php" => config_path('laravellocale.php')
        ],'config');
        $this->publishes([
            __DIR__."/../migration/2018_10_16_225525_create_locale_target.php" => database_path('migrations/2018_10_16_225525_create_locale_target.php'),
            __DIR__."/../migration/2018_10_16_225646_create_locale_content.php" => database_path('migrations/2018_10_16_225646_create_locale_content.php'),
        ],'migrations');
        $this->publishes([
            __DIR__."/../views/list.blade.php" => resource_path('views/vendor/lamb100/laravellocale/list.blade.php')
        ],'views');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
