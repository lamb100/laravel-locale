<!DOCTYPE html>
<html lang="{{env('APP_LOCALE','en')}}">
	<head>
		<!-- Required meta tags-->
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="Laravel Locale Designed By Lamb100 <lamb100.ad@gmail.com>">
	    <meta name="author" content="Lamb100">
	    <meta name="keywords" content="Locale">
	    <title>{{__(config('LaravelLocale.root').'.laravellocale.page.config.title')}}</title>
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	</head>
	<body>
		<div class="container-fluid">
			<nav class="navbar  navbar-expand-lg" v-if="data.length>0">
				<div class="collapse navbar-collapse">
					<ul class="navbar-nav mr-auto">
						<li><a href="#" @click="goPage($event,1)" title="{{__(config('LaravelLocale.root').'.paginate.first_page.title')}}"><i class="fas fa-step-backward"></i></a></li>
						<li><a href="#" @click="goPage($event,'-1')" title="{{__(config('LaravelLocale.root').'.paginate.previous_page.title')}}"><i class="fas fa-caret-left"></i></a></li>
						<li v-for="(i0,k0) in page"><a href="#" @click="goPage($event,i0)" v-if="i0.toString().match(/^[0-9]+$/)" v-html="i0"></a><span v-else v-html="i0"></span></li></li>
						<li><a href="#" @click="goPage($event,'+1')" title="{{__(config('LaravelLocale.root').'.paginate.next_page.title')}}"><i class="fas fa-caret-right"></i></a></li>
						<li><a href="#" @click="goPage($event,paginate.max)" title="{{__(config('LaravelLocale.root').'.paginate.last_page.title')}}"><i class="fas fa-step-forward"></i></a></li>
					</ul>
				</div>
			</nav>
			<div class="row">
				{{-- 選單功能區 --}}
				<div class="card col-4">
					<div class="card-header"><h4><a href="{{route('locale.generate')}}">{{__(config('LaravelLocale.root').'.laravellocale.function.generate')}}</a></h4></div>
					<div class="card-header"><h4><a href="{{route('locale.template')}}">{{__(config('LaravelLocale.root').'.laravellocale.function.template')}}</a></h4></div>
					<div class="card-header"><h4><a href="{{route('locale.scan')}}">{{__(config('LaravelLocale.root').'.laravellocale.function.scan')}}</a></h4></div>
					<div class="card-header"><h4><a href="{{route('locale.sync')}}">{{__(config('LaravelLocale.root').'.laravellocale.function.sync')}}</a></h4></div>
				</div>
				<div class="card col-4" v-for="(i0,k0) in process">
					<div class="card-header">
						<h4 v-html="i0.code"></h4>
					</div>
					<div class="card-body">
						<div class="card">
							<div class="card-header" v-for="(i1,k1) in supported"><a href="#" @click="toLocale($evnet,$k0,$k1)"><h4 :class="(i0.locale==i1?'bg-warning text-primary':'bg-secondary text-white')" v-html="i1"></h4></a></div>
							<div class="card-body">
								<div class="form-group row" v-if="i0.editable">
									<label class="col-sm-2 col-form-label">{{__(config('LaravelLocale.root').'.laravellocale.page.config.content.title')}}</label>
									<div class="col-sm-9">
										<input type="text" :class="'form-control myContent'+k0" :value="i0.content[i0.locale]"/>
									</div>
									<div class="col-sm-1">
										<button type="button" class="form-control btn-success" @click="saveLocale($event,k0)">{{__(config('LaravelLocale.root').'.laravellocale.page.config.save.btn')}}</button>
										<button type="button" class="form-control btn-danger" @click="undoEdit($event,k0)">{{__(config('LaravelLocale.root').'.laravellocale.page.config.undo.btn')}}</button>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-11" v-html="i0.content[i0.locale]"></div>
									<div class="col-sm-1">
										<button type="button" class="form-control btn-success" @click="editLocale($event,k0)">{{__(config('LaravelLocale.root').'.laravellocale.page.config.edit.btn')}}</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card col-4" v-if="data.length<=0"><div class="card-header"><h4>{{__(config('LaravelLocale.root').'.laravellocale.page.config.no_data.title')}}</h4></div></div>
			</div>
			<nav class="navbar  navbar-expand-lg" v-if="data.length>0">
				<div class="collapse navbar-collapse">
					<ul class="navbar-nav mr-auto">
						<li><a href="#" @click="goPage($event,1)" title="{{__(config('LaravelLocale.root').'.paginate.first_page.title')}}"><i class="fas fa-step-backward"></i></a></li>
						<li><a href="#" @click="goPage($event,'-1')" title="{{__(config('LaravelLocale.root').'.paginate.previous_page.title')}}"><i class="fas fa-caret-left"></i></a></li>
						<li v-for="(i0,k0) in page"><a href="#" @click="goPage($event,i0)" v-if="i0.toString().match(/^[0-9]+$/)" v-html="i0"></a><span v-else v-html="i0"></span></li></li>
						<li><a href="#" @click="goPage($event,'+1')" title="{{__(config('LaravelLocale.root').'.paginate.next_page.title')}}"><i class="fas fa-caret-right"></i></a></li>
						<li><a href="#" @click="goPage($event,paginate.max)" title="{{__(config('LaravelLocale.root').'.paginate.last_page.title')}}"><i class="fas fa-step-forward"></i></a></li>
					</ul>
				</div>
			</nav>
		</div>
		<div class="modalZone">
			<div v-for="(i0,k0) in sets" class="modal fade" :id="i0.id" tabindex="-1" role="dialog" :aria-labelledby="i0.title.id" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" :id="i0.title.id" v-html="i0.title.content"></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" v-if="i0.hasClose">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" v-html="i0.content">
							
						</div>
						<div class="modal-footer" v-if="i0.buttons.length>0">
							{{-- @todo --}}
							<button :type="(i1.type=='close'?'button':i1.type)" :data-dismiss="(i1.type=='close'?'modal':'')" v-for="(i1,k1) in i0.buttons" :class="'btn'+(i1.fa_class?' btn-'+i1.fa_class:'')">
								<i v-if="i1.fa_icon" :class="'fas fa-'+i1.fa_icon"></i>
								@{{i1.title}}
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="/css/app.css"/>
		<script src="/js/app.js"></script>
		<script>
			<!--//
			Vue.config.devtools = true
			var vModal=new Vue({
				el: '.modalZone',
				data: {
					sets: {},
					pointer:0,
				},
				methods: {
					open: function(options){
						switch(typeof options){
							case 'number':
								var id=vModel[options].id;
								vModel.pointer=options;
								break;
							case 'string':
								var id=options;
								var bExists=false;
								for(var i in vModel.sets){
									if(vModel.sets[i].id==id){
										vModel.pointer=i;
										break;
									}
								}
								break;
							case 'object':
								var id=options.id;
								vModel.pointer=vModel.length;
								vModel.sets.push(options)
							break;
						}
						$(id).modal('show');
					},
					close: function(pointer){
						switch(typeof options){
							case 'number':
								var id=vModel[options].id;
								vModel.pointer=options;
								break;
							case 'string':
								var id=options;
								var bExists=false;
								for(var i in vModel.sets){
									if(vModel.sets[i].id==id){
										vModel.pointer=i;
										break;
									}
								}
								break;
						}
						$(id).modal('hide');
					}
				}
			});
			var vMain=new Vue({
				el: '.container-fluid',
				data: {
					suooported:[],
					data:[],
					process:[],
					paginate:{
						min:0,
						max:0,
						now:0,
						rpp:20,
					},
					temp:{},
				},
				computed:{
					page: function(){
						var i=0;
						var hasDots=false;
						var aReturn=[];
						for(i=this.paginate.min;i<=this.paginate.max;i++){
							if(i<=3||i>=this.paginate.max-2||i==this.paginate.now){
								aReturn.push(i)
								hasDots=false;
							}else if(hasDots){
								aReturn.push('...');
								hasDots=true;
							}
						}
						return aReturn;
					}
				},
				methods:{
					saveLocale: function(e,k){
						var data={};
						data.code=vMain.process[k].code;
						data.locale=vMain.process[k].code;
						data.content=$('.myContent'+k,vMain.$el).val().trim();
						$.ajax({
							url: '{{route('locale.set')}}',
							method: 'post',
							data: data,
							dataType: 'json',
							beforeSend: function(){
								vMain.temp['body.css.cursor']=$('body').css('cursor');
								$('body').css('cursor','wait');
							},
							complete: function(){
								$('body').css('cursor',vMain.temp['body.css.cursor']);	
							},
							success: function(d){
								if(d.s){
									vMain.process[k].content[vMain.process[k].locale]=data.content;
									vMain.process[k].ediitable=false;
								}
							}
						});
					},
					undoEdit: function(e,k){
						vMain.process[k].ediitable=false;
					},
					editLocale: function(e,k){
						vMain.process[k].ediitable=true;
					},
					toLocale: function(e.dk,lk){
						vMain.process[dk].locale=vMain.supported[lk];
					}
					goPage: function(e,p){
						var t,nowAt;
						if(p&&p.toString().match(/^[0-9]+$/)){
							nowAt=vMain.paginate.now=parseInt(p);
						}else if(p&&p.toString().match(/^([\+\-])([0-9]+)$/)){
							t=p.toString().match(/^([\+\-])([0-9]+)$/);
							if(t[1]=='+'){
								vMain.paginate.now+=parseInt(t[2]);
							}else if($t[1]=='-'){
								vMain.paginate.now-=parseInt(t[2]);
							}
							if(vMain.paginate.now>vMain.paginate.max){
								vMain.paginate.now=vMain.paginate.max;
							}
							if(vMain.paginate.now<=0){
								vMain.paginate.now=1;
							}
							nowAt=vMain.paginate.now;
						}else{
							nowAt=vMain.paginate.now;
						}
						vMain.process=[];
						for(t=(nowAt-1)*vMain.paginate.rpp;t<nowAt*vMain.paginate.rpp;t++){
							if(vMain.data[t]){
								vMain.process.push(vMain.data[t]);
							}else{
								break;	
							}
						}
					}
				}
			});
			vMain.data=@json($data) ;
			vMain.supported=@json($supported) ;
			vMain.goPage();
			//-->
		</script>
	</body>
</html>